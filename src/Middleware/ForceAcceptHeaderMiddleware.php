<?php

namespace Dotcubed\LaravelApiException\Middleware;

use Closure;
use Illuminate\Http\Request;

class ForceAcceptHeaderMiddleware
{
    public function handle(Request $request, Closure $next): mixed
    {
        if (config('laravel-api-exception.header.force_accept')) {
            $request->headers->set('Accept', config('laravel-api-exception.header.accept_type'));
        }

        $response = $next($request);

        if (config('laravel-api-exception.header.force_accept')) {
            $response->headers->set('Accept', config('laravel-api-exception.header.accept_type'));
        }
        
        return $response;
    }
}
