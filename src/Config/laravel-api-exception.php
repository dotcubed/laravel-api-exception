<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Laravel API Exception
    |--------------------------------------------------------------------------
    |
    | Here you will be able to control how the exceptions are handled
    |
    */

    'exceptions' => [

        /**
         * Forces all errors in Larave to be handled by this package. This means even plain HTTP 
         * errors will be dealt with in this package. Use this only if your entire app only needs 
         * json returns (usually headless API projects) 
         */
        'blanket_handling' => false,
    ],

    'header' => [

        /**
         * Forces all requests to be injected with an explicit Accept header
         */
        'force_accept' => false,

        /**
         * The type of forced accept used for the request
         */
        'accept_type' => 'application/json',
    ],
];
