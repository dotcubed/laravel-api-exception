<?php

namespace Dotcubed\LaravelApiException\Exceptions;

use Exception;

class SimpleManualException extends Exception
{
    /**
     * @var string
     */
    public $message;

    public function __construct(string $message)
    {
        $this->message = $message;
    }
}
