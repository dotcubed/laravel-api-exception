<?php

namespace Dotcubed\LaravelApiException;

use Illuminate\Support\ServiceProvider;

class LaravelApiExceptionServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->app['router']->pushMiddlewareToGroup('api', \Dotcubed\LaravelApiException\Middleware\ForceAcceptHeaderMiddleware::class);

        $this->publishes(
            [
                __DIR__.'/Config/laravel-api-exception.php' => config_path('laravel-api-exception.php'),
            ],
            'laravel-api-exception',
        );
    }
    
    public function register()
    {
        $this->app->singleton(
            \Illuminate\Contracts\Debug\ExceptionHandler::class,
            \Dotcubed\LaravelApiException\Exceptions\Handler::class
        );
    }
}
